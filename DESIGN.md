
```haskell
|x * y|  = |x| * |y|
|x + y|  = |x| + |y|
|x -> y| = |y| ^ |x|

|∀ x. k|   = k, when k is free in x
|∀ x. f x| = |f 0|, when k is a functor in x
|∀ x. f x| = |f 1|, when k is a contravariant functor in x

|∀ x. Π fᵢ x| = Π |∀ x. fᵢ x|
|∀ x. Σ fᵢ x| = Σ |∀ x. fᵢ x|

|∀ x. k -> f x| = |k -> ∀ x. f x|, when k is free in x
```