package com.alexknvl.counting
import org.scalatest.PropSpec
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class CardinalityTests extends PropSpec with GeneratorDrivenPropertyChecks {
  property("cardinality operations") {
    import Cardinality.{Finite, Infinite}

    assert(Finite(1) + Finite(2) === Finite(3))
    assert(Finite(2) * Finite(2) === Finite(4))
    assert((Finite(3) ^ Finite(2)) === Finite(9))
    assert((Finite(0) ^ Finite(2)) === Finite(0))
    assert((Finite(3) ^ Finite(0)) === Finite(1))
    assert((Finite(0) ^ Finite(0)) === Finite(1))
    assert((Finite(1) ^ Finite(10)) === Finite(1))

    assert(Finite(1) + Infinite === Infinite)
    assert(Finite(1) * Infinite === Infinite)
    assert(Finite(0) * Infinite === Finite(0))
  }
}
