package com.alexknvl.counting
import scala.annotation.tailrec
import scala.annotation.unchecked.{ uncheckedVariance => uV }
import scala.collection.immutable.Queue


class Solver {

}
object Solver {
  // Given a problem, we can either
  //  * solve it in one step
  //  * produce more problems
  //  * decompose it into multiple subproblems
  sealed trait Result[+P, S] {
    import Result._
    def map(f: S => S): Result[P, S] = Cut(this, s => Done(f(s)))
    def flatMap[PP >: P](f: S => Result[PP, S]): Result[PP, S] = Cut(this, f)

    def zipWith[PP >: P](that: Result[PP, S])(f: (S, S) => S): Result[PP, S] =
      Cut[PP, S](this, s1 => Cut(that, s2 => Done(f(s1, s2))))
  }
  object Result {
    final case class Done[S](s: S) extends Result[Nothing, S]
    final case class Fork[P, S](l: LazyList[P]) extends Result[P, S]
    final case class Cut[P, S](p: Result[P, S], k: S => Result[P, S]) extends Result[P, S]

    def done[S](s: S): Result[Nothing, S] = Done(s)
    def cont[P, S](p: P): Result[P, S] = Fork(LazyList(p))
    def fail[S]: Result[Nothing, S] = Fork(LazyList.empty)
    def fork[P, S](p: LazyList[P]): Result[P, S] = Fork(p)

    def traverse[P, S](l: List[P], f: List[S] => S): Result[P, S] = l match {
      case Nil => Done(f(Nil))
      case x :: xs => Cut(cont(x), s => traverse(xs, ss => f(s :: ss)))
    }
  }

//  def and[P, S](f: P => Result[P, S], g: P => Result[P, S]): P => Result[P, S] =
//    p => Result.fork(f(p))

  def solve[P, S](init: P, max: Long, small: P => Result[P, S]): Option[S] = {
    val (_, r) = cut(small(init), max - 1, small)
    r
  }

  @tailrec def fork[P, S](queue: BankersQueue[P], visited: Set[P], steps: Long, max: Long, small: P => Result[P, S]): (Long, Option[S]) =
    if (steps >= max) (steps, None)
    else {
      queue.uncons match {
        case None => (steps, None)
        case Some((head, tail)) =>
          // println(s"Solver.fork: head = $head")
          small(head) match {
            case Result.Done(s) => (steps + 1, Some(s))

            case Result.Fork(l) =>
              fork(
                queue = tail snocReversed l.filter(p => p != head && !visited.contains(p)),
                visited = visited + head,
                steps + 1, max, small)

            case e@Result.Cut(_, _) => cut(e, max - steps - 1, small) match {
              case (c, Some(x)) => (steps + c, Some(x))
              case (c, None) => fork(tail, visited + head, steps + 1 + c, max, small)
            }
          }
      }
    }

  def cut[P, S](initial: Result[P, S], max: Long, small: P => Result[P, S]): (Long, Option[S]) =
    if (max <= 0) (0, None)
    else initial match {
      case Result.Done(s) => (0, Some(s))

      case Result.Fork(l) =>
        fork(BankersQueue.ofLazyList(l), Set.empty[P], 0, max - 1, small)

      case Result.Cut(p, k) =>
        val (c1, s1) = cut(p, max - 1, small)
        s1 match {
          case None    => (c1, None)
          case Some(s) =>
            val (c2, s2) = cut(k(s), max - c1, small)
            (c1 + c2, s2)
        }
    }
}
