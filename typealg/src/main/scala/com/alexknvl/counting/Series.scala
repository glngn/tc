package com.alexknvl.counting

import cats.instances.list._

// Σ aₖ xᵏ represented as a map k ↦ aₖ
sealed trait Series[E] {
  import Series._

  // (Σ aₖ xᵏ) + (Σ bₖ xᵏ)
  def + (that: Series[E])(implicit E: Semiring[E]): Series[E] =
    (this, that) match {
      case (Simple(a), Simple(b)) =>
        Simple((a.keySet ++ b.keySet).map { k =>
          k -> E.sum(a.get(k).toList ++ b.get(k).toList)
        }.toMap)
    }

  // (Σ aₖ xᵏ) * (Σ bₖ xᵏ)
  def * (that: Series[E])(implicit E: Semiring[E]): Series[E] =
    (this, that) match {
      case (Simple(a), Simple(b)) =>
        val values = for {
          (k1, a1) <- a.iterator
          (k2, a2) <- b
        } yield E.add(k1, k2) -> E.mul(a1, a2)

        Series.of(values.toSeq:_*)
    }

  def transform(f: (E, E) => (E, E))(implicit E: Semiring[E]): Series[E] =
    this match {
      case Simple(a) =>
        Series.of(
          a.iterator.map { case (k, v) => f(k, v) }
          .toSeq : _*
        )
    }

  // ∀ x. f x -> g x
  // ~ ∀ x. (Σᵢ fᵢ x) -> g x
  // ~ ∀ x. (Σᵢ bᵢ * (aᵢ -> x)) -> g x
  // ~ ∀ x. Πᵢ (bᵢ -> (aᵢ -> x) -> g x)
  // ~ Πᵢ (∀ x. (aᵢ -> x) -> bᵢ -> g x)
  // ~ Πᵢ bᵢ -> g aᵢ
  def yoneda(g: E => E)(implicit E: PowerSemiring[E]): E = this match {
    case Simple(terms) =>
      E.product(
        terms.iterator
          .map { case (a, b) => E.pow(g(a), b) }
          .toList)
  }
}
object Series {
  final case class Simple[E](values: Map[E, E]) extends Series[E]

  def zero[E]: Series[E] =
    Simple(Map.empty)

  def unit[E](implicit E: Semiring[E]): Series[E] =
    Simple(Map(E.zero -> E.unit))

  def const[E](value: E)(implicit E: Semiring[E]): Series[E] =
    Simple(Map(E.zero -> value))

  def single[E](implicit E: Semiring[E]): Series[E] =
    Simple(Map(E.unit -> E.unit))

  def of[E](values: (E, E)*)(implicit E: Semiring[E]): Series[E] =
    Simple(
      values.foldLeft(Map.empty[E, E]) { case (acc, (k, v)) =>
        acc.get(k) match {
          case None     => acc + (k -> v)
          case Some(v0) => acc + (k -> E.add(v0, v))
        }
      }
    )

  def sum[E](l: List[Series[E]])(implicit E: Semiring[E]): Series[E] =
    l.foldLeft(zero[E])(_ + _)

  def product[E](l: List[Series[E]])(implicit E: Semiring[E]): Series[E] =
    l.foldLeft(unit[E])(_ * _)
}
