package com.alexknvl.counting
import cats.Monad

import scala.annotation.tailrec

object `package` {
  implicit val intSemiring: PowerSemiring[Int] = new PowerSemiring[Int] {
    override def zero: Int = 0
    override def unit: Int = 1
    override def add(x: Int, y: Int): Int = x + y
    override def mul(x: Int, y: Int): Int = x * y
    override def pow(x: Int, y: Int): Int = {
      @tailrec def go(k: Int, n: Int): Int =
        if (k < y) go(k + 1, n * x)
        else n
      go(0, 1)
    }
  }

//  implicit val setMonad: Monad[Set] =
//    new Monad[Set] {
//      def pure[A](a: A): Set[A] = Set(a)
//      override def map[A, B](fa: Set[A])(f: A => B): Set[B] = fa.map(f)
//      def flatMap[A, B](fa: Set[A])(f: A => Set[B]): Set[B] = fa.flatMap(f)
//
//      def tailRecM[A, B](a: A)(f: A => Set[Either[A, B]]): Set[B] = {
//        val bldr = Set.newBuilder[B]
//
//        @annotation.tailrec def go(set: Set[Either[A, B]]): Unit = {
//          val lefts = set.foldLeft(Set[A]()) { (memo, either) =>
//            either.fold(
//              memo + _,
//              b => {
//                bldr += b
//                memo
//              }
//            )
//          }
//          if (lefts.isEmpty)
//            ()
//          else
//            go(lefts.flatMap(f))
//        }
//        go(f(a))
//        bldr.result()
//      }
//    }
}
