package com.alexknvl.counting
import atto.ParseResult
import cats.data.NonEmptyList

trait ExprAlg[F, T] {
  type From = F
  type To = T

  def fin(value: Cardinality): To
  def prod(left: From, right: From): To
  def sum(left: From, right: From): To
  def arr(from: From, to: From): To

  def use(value: Int): To
  def forall(value: From): To
}

object ExprAlg {
  val depth: ExprAlg[Int, Int] = new ExprAlg[Int, Int] {
    def fin(value: Cardinality): To       = 0
    def prod(left: From, right: From): To = (left max right) + 1
    def sum(left: From, right: From): To  = (left max right) + 1
    def arr(left: From, right: From): To  = (left max right) + 1
    def use(value: Int): To               = 0
    def forall(value: From): To           = value + 1
  }

  val nodes: ExprAlg[Int, Int] = new ExprAlg[Int, Int] {
    def fin(value: Cardinality): To       = 1
    def prod(left: From, right: From): To = (left + right) + 1
    def sum(left: From, right: From): To  = (left + right) + 1
    def arr(left: From, right: From): To  = (left + right) + 1
    def use(value: Int): To               = 1
    def forall(value: From): To           = value + 1
  }

  val closed: ExprAlg[Int => Boolean, Int => Boolean] = new ExprAlg[Int => Boolean, Int => Boolean] {
    def fin(value: Cardinality): To       = _ => true
    def prod(left: From, right: From): To = d => left(d) && right(d)
    def sum(left: From, right: From): To  = d => left(d) && right(d)
    def arr(left: From, right: From): To  = d => left(d) && right(d)
    def use(value: Int): To               = d => value < d
    def forall(value: From): To           = d => value(d + 1)
  }

  class ExprParser[R](alg: ExprAlg[R, R]) {
    import atto._
    import Atto._

    type Ctx = List[String]
    type P[A] = Ctx => Parser[A]

    val and: Parser[String]    = token(string("*")).namedOpaque("AND")
    val or: Parser[String]     = token(string("+")).namedOpaque("OR")
    val arr: Parser[String]    = token(string("->")).namedOpaque("ARR")
    val forall: Parser[String] = token(string("∀") | string("forall") | string("\\/")).namedOpaque("FORALL")
    val dot: Parser[String]    = token(string(".")).namedOpaque("DOT")

    val ident: Parser[String] =
      (letter ~ letterOrDigit.many).map {
        case (first, rest) => first + rest.mkString
      }

    val cadinality: Parser[Cardinality] =
      bigInt.map(Cardinality.Finite) | string("∞").map(_ => Cardinality.Infinite)

    ////////////////////////////////////////////////////////////////////////////

    def forallExpr(env: Ctx): Parser[R] = {
      (forall ~> token(ident).many1 <~ dot).flatMap { vars =>
        forallExpr(vars.reverse.toList ++ env).map { r => vars.foldLeft(r)((e, _) => alg.forall(e)) }
      } | arrExpr(env)
    }.named("forall")

    def arrExpr(env: Ctx): Parser[R] = {
      (orExpr(env) ~ opt(arr ~> forallExpr(env))).map {
        case (a, Some(b)) => alg.arr(a, b)
        case (a, None) => a
      }
    }.named("arr")

    def orExpr(env: Ctx): Parser[R]  = andExpr(env).sepBy1(or).map(_.reduceLeft(alg.sum))
    def andExpr(env: Ctx): Parser[R] = value(env).sepBy1(and).map(_.reduceLeft(alg.prod))

    def useExpr(env: Ctx): Parser[R] = {
      token(ident).flatMap[R] { name =>
        val i = env.indexOf(name)
        if (i == -1) {
          Atto.err(s"unknown var name $name")
        } else Atto.ok(alg.use(i))
      }
    }.named("use")

    def value(env: Ctx): Parser[R] = {
      (
        (token(string("(")) ~> forallExpr(env) <~ token(string(")"))).named("paren")
          | token(cadinality.map(alg.fin)).namedOpaque("fin")
          | useExpr(env)
        )
    }.named("value")

    val line: Parser[R] = skipWhitespace ~> forallExpr(Nil) <~ skipWhitespace <~ endOfInput
  }

  def parser[R](alg: ExprAlg[R, R]): String => ParseResult[R] = {
    import atto._
    import Atto._

    s => new ExprParser[R](alg).line.parseOnly(s).done
  }
}