package com.alexknvl.algebra
import scala.reflect.macros.blackbox

trait Initial[F[_, _]] {
  def cata[Z](f: F[Z, Z]): Z
}
object Initial {
//  def algebra[F[_, _]]: F[Initial[F], Initial[F]] = macro Macros.algebra[F]
//
//  private final class Macros(c: blackbox.Context) {
//
//    def algebra[F[_, _]](implicit F: c.WeakTypeTag[F[_, _]]): c.Tree = {
//      println(c.weakTypeTag[F[_, _]])
//      ???
//    }
//  }
}
