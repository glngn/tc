addSbtPlugin("org.wartremover" % "sbt-wartremover" % "2.3.5")
addSbtPlugin("org.xerial.sbt"  % "sbt-pack"        % "0.11")
addSbtPlugin("com.geirsson"    % "sbt-scalafmt"    % "1.5.1")